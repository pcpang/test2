/**
 * Created by pohchye on 26/7/2016.
 */

// Initialize Express
var express = require("express");
var app = express();

// Initialize Body-Parser to parse body parameters for POST method
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({"extended": "true"}));
app.use(bodyParser.json());

// Use promise on Express
// var queue = require("q");
// var q = require("q");
var q = require("q");

// Initialize MySql connection
var mysql = require("mysql");
// Create mysql connection pool to database
var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "pohchye",
    password: "pohchye",
    database: "books",
    connectionLimit: 4
});
// console.log("Pool:", pool);

// --- Initialize Variables ---
const SQL_SEARCH_BOOK = "select * from books where (author_lastname like ?  OR author_firstname like ?) AND title like ? limit 10";
const SQL_GET_BOOK = "select id,author_lastname,author_firstname,title,cover_thumbnail from books where id=?";
const SQL_UPDATE_BOOK = "update books set author_lastname=?,author_firstname=?,title=?,cover_thumbnail=? where id = ?";
// --- End of Initialize Variables ---

// --- Middleware ----

// Book Search
app.get("/api/books/search", function (req, res) {
    console.log("* Executing API (/api/books/search)");
    // Retrieve parameters from request
    console.log("> Get Received Book Title: " + req.query.title);
    console.log("> Get Received Book Author: " + req.query.author);
    var title = "%" + req.query.title + "%";
    var author = "%" + req.query.author + "%";
    // Format argument to pass to SQL query
    console.log("> Formatted Book Parameters:", title, author);
    var ARGS = [author, author, title];
    // Retrieve book list from database based on criteria
    pool.getConnection(function (err, connection) {
        if (err) {
            console.log("Error in connection to SQL database:", err);
            res.status(500).end("Error in connection to SQL database: " + err);
        }
        // If no connection error, perform SQL query
        connection.query(SQL_SEARCH_BOOK, ARGS,
            function (err, result) {
                connection.release();
                if (err) {
                    console.log("Error in book search:", err);
                    res.status(500).end("Error in book search: " + err);
                }
                console.log("Success in book search:", result);
                res.status(202).json(result);
            }
        );
    });
});
// End of Book Search

// Get book
app.get("/api/book/get/:bookId", function (req, res) {
    console.log("* Executing API (/api/books/search/:bookId)");
    console.log("> Post Received Param: " + req.params.bookId);
    var ARGS = [req.params.bookId];
    pool.getConnection(function (err, connection) {
        if (err) {
            console.log("Error in connection to SQL database:", err);
            res.status(500).end("Error in connection to SQL database: " + err);
        }
        // If no connection error, perform SQL delete
        connection.query(SQL_GET_BOOK, ARGS,
            function (err, result) {
                connection.release();
                if (err) {
                    console.log("Error in getting book:", req.params.bookId);
                    res.status(500).end("Error for getting book API: " + err);
                }
                console.log("Success in getting book:", req.params.bookId);
                res.status(202).json(result[0]);
            }
        );
    });
});
// End of Get book

// Update book
app.post("/api/book/update", function (req, res) {
    console.log("* Executing API (/api/book/update)");
    // Extract task id from request
    var book = JSON.parse(req.body.params.book);
    console.log("Book to update:", book.id, book.title, book.author_lastname);
    var ARGS = [book.author_lastname,book.author_firstname,book.title,book.cover_thumbnail,book.id];
    console.log("ARGS:", ARGS);
    pool.getConnection(function (err, connection) {
        if (err) {
            return errorCallback(err);
        }
        // If no connection error, perform SQL delete
        connection.query(SQL_UPDATE_BOOK, ARGS,
            function (err, result) {
                connection.release();
                if (err) {
                    console.log("Error in modifying book:", book.id, book.title, book.author_lastname);
                    res.status(500).end("Error for updating book API: " + err);
                }
                console.log("Success in modifying book:", book.id, book.title, book.author_lastname);
                res.status(202).send("Success for modifying book: " + book.title);
            }
        );
    });
});
// End of Update book

// Serving public files
app.use(express.static(__dirname + "/public"));
// Serving bower modules
app.use("/bower_components", express.static(__dirname + "/bower_components"));
// Handle anything that does not match
app.use(function (req, res) {
    res.redirect("error.html");
});

// --- End of Middleware ----


// Set the port
// app.set("port", process.argv[2] || process.env.ARGV_PORT || 3001);
app.set("port", 3000);

// Start the express server
app.listen(app.get("port"), function () {
    console.info("Express server started on port", app.get("port"));
});


