/**
 * Created by pohchye on 26/7/2016.
 */

(function () {
        
    angular.module("BooksApp")
        .controller("BooksCtrl", BooksCtrl)
        .controller("ModifyCtrl", ModifyCtrl);

    // Book Search Controller
    function BooksCtrl(BooksDBSvc) {
        console.log("Executing Controller BooksCtrl");

        // Create the controller handler
        var vm = this;

        // --- Initialize Variables - Model (data bind variables to View HTML) ---
        // Initialize the book object
        vm.book = BooksDBSvc.getBookObject();
        // Book
        vm.books = [];
        vm.searchLength = "";
        // Status
        vm.status = {
            message: "",
            code: 0
        };
        // --- End of Initialize Variables ---

        // --- Define the actions - Model (data bind actions to View HTML) ----
        // Book Search
        vm.search = function () {
            console.log("* Executing BooksCtrl function: search");
            console.log("> Search Title:", vm.book.title);
            console.log("> Search Author:", vm.book.author);
            // Execute the book search service
            BooksDBSvc.search(vm.book)
                .then(function (result) {
                    console.log("> Controller Search Result:", result);
                    // Update book search result to books array
                    vm.books = result; // to display on search content page
                    vm.searchLength = result.length; // to display number of books found that match criteria
                    console.log("List of Books:", vm.books);
                    // Set success status
                    vm.status.message = "Success in book search";
                    vm.status.code = "202";
                    // Re-Initialize Book after a search
                    // vm.book = BooksDBSvc.getBookObject();
                })
                .catch(function (err) {
                    console.log("> Controller Search Error:", err);
                    // Set error status
                    vm.status.message = "Error in book search";
                    vm.status.code = "400";
                });
        };
        // End of Book Search
        // --- End of Define the actions ---
    } // End of BooksCtrl

    // Book Modify Controller
    function ModifyCtrl($stateParams, BooksDBSvc) {
        console.log("Executing Controller ModifyCtrl");
        // Create the controller handler
        var vm = this;
        // --- Initialize Variables - Model (data bind variables to View HTML) ---
        // Initialize the book object
        vm.modifiedBook = BooksDBSvc.getBookObject();
        // Status
        vm.status = {
            message: "",
            code: 0
        };
        // --- End of Initialize Variables ---

        // Get the modified book id
        console.log("Book ID:", $stateParams.id);
        // Retrieve book details
        BooksDBSvc.getBook($stateParams.id)
            .then(function (result) {
                console.log("> Controller Get Book Result:", result);
                // Get modified book details and update to model
                vm.modifiedBook = result; // to display on modified page
                console.log("> Modified Book Details:", vm.modifiedBook);
                // Set success status
                vm.status.message = "Success in retrieving book details";
                vm.status.code = "202";
                // Re-Initialize Book after a search
                // vm.modifiedBook = BooksDBSvc.getBookObject();
            })
            .catch(function (err) {
                console.log("> Controller Get Book Error:", err);
                // Set error status
                vm.status.message = "Error in retrieving book details";
                vm.status.code = "400";
            });

        // --- Define the actions - Model (data bind actions to View HTML) ----
        // Modify Book
        vm.modify = function () {
            console.log("* Executing ModifyCtrl function: modify");
            BooksDBSvc.modify(vm.modifiedBook)
                .then(function (result) {
                    console.log("> Controller Result:", result);
                    // Set success status
                    vm.status.message = "Success in modifying book";
                    vm.status.code = "202";
                })
                .catch(function (err) {
                    console.log("> Controller Error:", err);
                    // Set error status
                    vm.status.message = "Failure in modifying book";
                    vm.status.code = "400";
                });
        };
        // End of Modify Book
        // --- End of Define the actions ---
    } // End of ModifyCtrl

    // Inject controller with service
    BooksCtrl.$inject = ["BooksDBSvc"];
    ModifyCtrl.$inject = ["$stateParams", "BooksDBSvc"];
    
})();
