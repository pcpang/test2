/**
 * Created by pohchye on 26/7/2016.
 */

(function () {
    angular.module("BooksApp")
        .service("BooksDBSvc", BooksDBSvc);

    // Inject service with http and q
    BooksDBSvc.$inject = ["$http", "$q"];

    function BooksDBSvc($http, $q) {
        console.log("Executing Service BooksDBSvc");
        // Create the service handler
        var service = this;

        // --- Initialize Variables ---
        service.bookList = [];
        // --- End of Initialize Variables ---

        // --- Define the actions ----

        // Book Search
        service.search = function (searchBook) {
            console.log("* Executing BooksDBSvc function: search");
            console.log("Search Book:", searchBook);
            // Create the promise handler
            var defer = $q.defer();
            console.log(searchBook.title, searchBook.author);
            // Call search API on Express
            $http.get("/api/books/search", { params: {
                    title: searchBook.title,
                    author: searchBook.author
                }}
            ).then(function (result) {
                console.log("> Service Result:", result.data);
                return defer.resolve(result.data);
            }).catch(function (err) {
                console.log("> Service Error:", err);
                return defer.reject(err);
            });
            return defer.promise;
        };
        // End of Book Search
        
        // Get Book Details
        service.getBook = function (bookId) {
            console.log("* Executing BooksDBSvc function: getBook");
            console.log("Book ID:", bookId);
            // Create the promise handler
            var defer = $q.defer();
            // Call get book API on Express
            $http.get("/api/book/get/" + bookId
            ).then(function (result) {
                console.log("> Service Result:", result.data);
                return defer.resolve(result.data);
            }).catch(function (err) {
                console.log("> Controller Error:", err);
                return defer.reject(err);
            });
            return defer.promise;
        };
        // End of Get Book Details

        // Modify Book
        service.modify = function (modifyBook) {
            console.log("* Executing BooksDBSvc function: modify");
            console.log("Book to update:", modifyBook);
            // Create the promise handler
            var defer = $q.defer();
            // Call modify task API on Express
            $http.post("/api/book/update", {
                params: {
                    book: JSON.stringify(modifyBook),
                    headers: {'Content-Type': 'application/json'}
                }
            }).then(function (result) {
                console.log("> Service Result:", result.data);
                // Return result to controller
                return defer.resolve(result.data);
            }).catch(function (err) {
                console.log("> Service Error:", err);
                return defer.reject(err.status);
            });
            return defer.promise;
        };
        // End of Modify Book
        
        // Initialize the book object
        service.getBookObject = function () {
            console.log("* Executing BooksDBSvc function: getBookObject");
            var book = {};
            book.id = "";
            book.author_lastname = "";
            book.author_firstname = "";
            book.title = "";
            book.cover_thumbnail = "";
            return book;
        };
        // End of Initialize the book object
        // --- End of Define the actions ----
    } // End of Service BooksDBSvc
})();