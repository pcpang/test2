/**
 * Created by pohchye on 26/7/2016.
 */

(function () {

    angular
        .module("BooksApp")
        .config(BooksConfig);

    function BooksConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("search", {
                url: "/",
                templateUrl: "search.html",
                controller: "BooksCtrl as ctrl"
            })
            .state("modify", {
                url: "/modify/:id",
                templateUrl: "modify.html",
                controller: "ModifyCtrl as ctrl"
            });

        $urlRouterProvider.otherwise("/");
    }

    BooksConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
})();
